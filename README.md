# README

## Introduction

This project holds the HTML, CSS and JavaScript that provides a map and user interface for exploring
the OutStories content held by the Bristol City Council ArcGIS system. This code is shared between
a WordPress plugin used on the OutStories website and iOS and Android applications used by the
Apache Cordova framework.

There are a number of grunt targets (dev, app and wp) which compiles the SASS to CSS, copies 
JavaScript and inserts variables (e.g. MapBox api keys) for local development, WordPress plugin
and app. The WordPress plugin and app projects will copy this assets into their own space
before distribution.

## Installation

To install the dependencies:

    npm install

## Project structure

~~~
src/                Source files that need copying to targets
src/image           Markers etc.
src/js              JavaScript files
src/lib             Third party JavaScript and CSS
src/scss            SASS files
platform/test/      Map demo; index.html which uses the JavaScript and CSS under src
platform/ios_app/   App distribution (iOS and Android); index.html which uses the JavaScript and CSS under src
platform/wp/        WordPress distribution; index.html which uses the JavaScript and CSS under src
~~~

You also need a config.properties file at the root of the project with the following properties with
values:

~~~
mapbox_api_dev          MapBox API key for development
mapbox_api_wp           MapBox API key for WordPress
mapbox_api_app          MapBox API key for iOS and Android apps
~~~

For example:

~~~
mapbox_api_dev=7f493b0899564ab5ad2349ee1b4ed708
mapbox_api_wp=b422a8fd871942189c8bf0d511871ed0
mapbox_api_app=53632e0e57c4411eb6770955d420561d
~~~

## Commands

To compile the style sheets and copy the JS to run with index.html file under test:

~~~
grunt dev       Copy files into test
grunt app       Copy files into app
~~~