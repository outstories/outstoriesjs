module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        app: {
            name: 'outstories-wp-plugin'
        },
        properties: {
            cfg: 'config.properties'
        },
        replace: {
            dev_token: {
                options: {
                    patterns: [
                    {
                        match: 'mapbox_api_key',
                        replacement: '<%= cfg.mapbox_api_dev %>'
                    }]
                },
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: ['platform/test/js/osb_layers.js'],
                        dest: 'platform/test/js/'
                    }
                ]
            },
            wp_token: {
                options: {
                    patterns: [
                        {
                            match: 'mapbox_api_key',
                            replacement: '<%= cfg.mapbox_api_wp %>'
                        }]
                },
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: ['platform/wp/js/osb_layers.js'],
                        dest: 'platform/wp/js/'
                    }
                ]
            },
            app_token: {
                options: {
                    patterns: [
                        {
                            match: 'mapbox_api_key',
                            replacement: '<%= cfg.mapbox_api_app %>'
                        }]
                },
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: ['platform/ios_app/js/osb_layers.js'],
                        dest: 'platform/ios_app/js/'
                    }
                ]
            }
        },
        sass: {
            dist: {
                options: {
                    sourcemap: 'none'
                },
                files: {
                    'platform/test/css/osbjs.css': 'src/scss/main.scss'
                }
            },
            dist_wp: {
                options: {
                    sourcemap: 'none'
                },
                files: {
                    'platform/wp/css/osbjs.css': 'src/scss/main_wp.scss'
                }
            },
            dist_app: {
                options: {
                    sourcemap: 'none'
                },
                files: {
                    'platform/ios_app/css/osbjs.css': 'src/scss/main_app.scss'
                }
            }
        },
        jshint: {
            all: {
                src: ['Gruntfile.js', 'src/js/**/*.js']
            }
        },
        copy: {
            copy_dev: {
                files: [
                    {
                        cwd: 'src/lib/css',
                        src: ['**'],
                        dest: 'platform/test/css',
                        expand: true
                    },
                    {
                        cwd: 'src/lib/js',
                        src: ['**'],
                        dest: 'platform/test/js',
                        expand: true
                    },
                    {
                        cwd: 'src/lib/css',
                        src: ['**'],
                        dest: 'platform/test/css',
                        expand: true
                    },
                    {
                        cwd: 'src/js',
                        src: ['osbjs.js', 'osb_layers.js'],
                        dest: 'platform/test/js',
                        expand: true
                    },
                    {
                        cwd: 'src/image',
                        src: ['**'],
                        dest: 'platform/test/image',
                        expand: true
                    }
                ]
            },
            copy_app: {
                files: [
                    {
                        cwd: 'src/lib/css',
                        src: ['**'],
                        dest: 'platform/ios_app/css',
                        expand: true
                    },
                    {
                        cwd: 'src/lib/js',
                        src: ['**'],
                        dest: 'platform/ios_app/js',
                        expand: true
                    },
                    {
                        cwd: 'src/lib/css',
                        src: ['**'],
                        dest: 'platform/ios_app/css',
                        expand: true
                    },
                    {
                        cwd: 'src/js',
                        src: ['osbjs.js', 'osb_layers.js', 'app.js'],
                        dest: 'platform/ios_app/js',
                        expand: true
                    },
                    {
                        cwd: 'src/image',
                        src: ['marker.png', 'marker@2x.png','osb-background.png','spiffygif_30x30.gif',
                              'spiffygif_30x30_wbg.gif','caret-left.png'],
                        dest: 'platform/ios_app/image',
                        expand: true
                    }
                ]
            },
            copy_wp: {
                files: [
                    {
                        cwd: 'src/lib/css',
                        src: ['**'],
                        dest: 'platform/wp/css',
                        expand: true
                    },
                    {
                        cwd: 'src/lib/js',
                        src: ['leaflet.js', 'leaflet.markercluster.js'],
                        dest: 'platform/wp/js',
                        expand: true
                    },
                    {
                        cwd: 'src/css',
                        src: ['**'],
                        dest: 'platform/wp/css',
                        expand: true
                    },
                    {
                        cwd: 'src/js',
                        src: ['osbjs.js', 'osb_layers.js'],
                        dest: 'platform/wp/js',
                        expand: true
                    },
                    {
                        cwd: 'src/image',
                        src: ['marker.png', 'marker@2x.png','osb-background.png','spiffygif_30x30.gif',
                              'spiffygif_30x30_wbg.gif', 'plus-square.png', 'minus-square.png',
                              'osb-logo.png', 'uob-logo.png', 'BCC-logo.png', 'ahrc-logo.png'],
                        dest: 'platform/wp/image',
                        expand: true
                    }
                ]
            }
        },
        watch: {
            css: {
                files: '**/*.scss',
                tasks: ['sass'],
                options: {
                    cwd: 'src/scss'
                }
            },
            files: {
                files: ['**/*.css', '**/*.js'],
                tasks: ['dev-newer'],
                options: {
                    cwd: 'src/plugin'
                }
            }
        }
    });

    // we can use a properties file
    grunt.loadNpmTasks('grunt-properties-reader');

    // compression
    grunt.loadNpmTasks('grunt-contrib-compress');

    // sass for css
    grunt.loadNpmTasks('grunt-contrib-sass');

    // check our JavaScript
    grunt.loadNpmTasks('grunt-contrib-jshint');

    // copy our files into place
    grunt.loadNpmTasks('grunt-contrib-copy');

    // watch files
    grunt.loadNpmTasks('grunt-contrib-watch');

    // sync newer files (when watching)
    grunt.loadNpmTasks('grunt-newer');

    // text replacement
    grunt.loadNpmTasks('grunt-replace');

    // default task(s).
    grunt.registerTask('default', ['properties', 'sass', 'jshint']);

    // copy wp dev environment
    grunt.registerTask('dev', ['default', 'copy:copy_dev', 'replace:dev_token']);

    // app
    grunt.registerTask('app', ['default', 'sass:dist_app', 'copy:copy_app', 'replace:app_token']);

    // wp
    grunt.registerTask('wp', ['default', 'sass:dist_wp', 'copy:copy_wp', 'replace:wp_token']);


};