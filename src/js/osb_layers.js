osb_layers = {

    layers: [
        {
            id: "mapbox",
            name: "MapBox",
            layer: "https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/256/{z}/{x}/{y}?access_token=@@mapbox_api_key",
            attribution: "&copy; <a href='https://www.mapbox.com/map-feedback/'>Mapbox</a> &copy; <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a>",
            maxZoom: 18,
            minZoom: 5
        },
        {
            id: "stamen",
            name: "Stamen Toner",
            layer: "http://tile.stamen.com/toner/{z}/{x}/{y}.png",
            attribution: "Map tiles by <a href='http://stamen.com'>Stamen Design</a>, under <a href='http://creativecommons.org/licenses/by/3.0'>CC BY 3.0</a>. Data by <a href='http://openstreetmap.org'>OpenStreetMap</a>, under <a href='http://www.openstreetmap.org/copyright'>ODbL</a>.",
            maxZoom: 18,
            minZoom: 5
        },
        {
            id: "bro1949",
            name: "1949",
            layer: 'https://maps.bristol.gov.uk/ArcGIS/rest/services/BaseMaps/1949_epsg3857/MapServer/tile/{z}/{y}/{x}/',
            attribution: "&copy; <a href='http://www.bristol.gov.uk/page/leisure-and-culture/records-and-archives'>Bristol Record Office</a>",
            maxZoom: 18,
            minZoom: 5
        },
        {
            id: "bro1900",
            name: "1900",
            layer: 'https://maps.bristol.gov.uk/ArcGIS/rest/services/BaseMaps/bcc_cs_e2_col_epsg3857/MapServer/tile/{z}/{y}/{x}/',
            attribution: "&copy; <a href='http://www.bristol.gov.uk/page/leisure-and-culture/records-and-archives'>Bristol Record Office</a>",
            maxZoom: 18,
            minZoom: 5
        },
        {
            id: "bro1828",
            name: "1828 Ashmead",
            layer: 'https://maps.bristol.gov.uk/ArcGIS/rest/services/BaseMaps/1828_epsg3857/MapServer/tile/{z}/{y}/{x}/',
            attribution: "&copy; <a href='http://www.bristol.gov.uk/page/leisure-and-culture/records-and-archives'>Bristol Record Office</a>",
            maxZoom: 18,
            minZoom: 5
        }
    ]
};