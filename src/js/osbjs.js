var osb = {

    /**
     * Initialise the object - get data and populate the map.
     *
     * @param mapper - object for mapping results to objects we can use.
     * @param map - reference to the LeafletJS map object.
     * @param layers - reference t
     */
    init: function (mapper, map, layers, icon) {
        osb.config.mapper = mapper;
        osb.map.map = map;
        osb.map.layers = layers;
        osb.map.icon = ((icon !== null) ? icon : osb.icon());
        osb.populateAll();
        osb.initSideBar();

    },

    /**
     * Encapsulates configuration.
     */
    config: {
        // Cache key for the raw data
        rawDataCacheKey: 'osb-raw-data-cache',
        // Cache key for an individual place
        placeDataCacheKey: 'osb-place-cache',
        mediaDataCacheKey: 'osb-place-media-cache',
        // cache lasts 30 minutes
        dataCacheExpire: 30 * 60 * 1000,
        // max number of places to cache
        maxCacheItems: 10,
        // mapper code object
        mapper: null,
        // characters before we truncate (in popup)
        truncate: 150,
        // tag soup threshold
        tagSize: [
            {
                val: 2,
                tag: 'osb-tag-a'
            },
            {
                val: 4,
                tag: 'osb-tag-b'
            },
            {
                val: 8,
                tag: 'osb-tag-c'
            },
            {
                val: 16,
                tag: 'osb-tag-c'
            },
            {
                val: 32,
                tag: 'osb-tag-c'
            }
        ],
        tagStyle: {
            THRESH_A: 'osb-tag-a',
            THRESH_B: 'osb-tag-a',
            THRESH_C: 'osb-tag-c',
            THRESH_D: 'osb-tag-d',
            THRESH_E: 'osb-tag-e',
            THRESH_F: 'osb-tag-f'
        }

    },

    /**
     * Map related objects.
     */
    map: {
        map: null,
        layers: null,
        currentLayer: null,
        selectedPopup: null,
        markers: null,
        icon: null,
        createLayer: function (layerDetails) {
            return L.tileLayer(layerDetails.layer, {
                attribution: layerDetails.attribution, id: layerDetails.id,
                maxZoom: layerDetails.maxZoom, minZoom: layerDetails.minZoom
            });
        }
    },

    /**
     * Data objects.
     */
    data: {
        places: []
    },

    keywords: {
        data: {},
        threshold: function (size) {
            for (var i = 0; i < osb.config.tagSize.length; i++) {
                if (size <= osb.config.tagSize[i].val) {
                    return osb.config.tagSize[i].tag;
                }
            }
            return 'osb-tag-d';
        }
    },

    icon: function () {
        return L.icon({
            iconUrl: './image/marker.png',
            iconRetinaUrl: './image/marker@2x.png',
            iconSize: [32, 47],
            iconAnchor: [16, 46],
            popupAnchor: [0, -55]
        });
    },

    /**
     * Populate the map with markers. We need to query the remote server to get basic information on
     * all of the available points. We check to see of we have acceptable data in the cache. Don't want
     * to cache? Pass null for the cacheKey.
     */
    populate: function (cacheKey, dataUrl) {

        // show we are doing work
        // jQuery('#osb-map-message').show();
        jQuery('#osb-keyword-list').html('<p>Updating keywords ...</p>');

        // get data from cache
        if (cacheKey !== null && osb.cache.get(cacheKey) !== null) {
            osb.handleRawData(osb.cache.get(cacheKey));
        } else {
            // get data
            osb.endpoint.getData(dataUrl).done(function (data) {
                if (cacheKey !== null) {
                    if (!data.error) {
                        osb.cache.add(cacheKey, data);
                    }
                }
                osb.handleRawData(data);
            }).error(function (x, y) {
                console.log(x);
                console.log(y);
            });
        }

    },

    /**
     * Get data for all of the places - cached.
     */
    populateAll: function () {

        jQuery('#osb-map-toggle').on('click', function () {
            if (jQuery(this).data('state') === 'min') {
                jQuery(this).removeClass('osb-map-min').addClass('osb-map-max');
                jQuery('article').addClass('osb-max-expand');
                jQuery(this).data('state', 'max');
            } else {
                jQuery('article').removeClass('osb-max-expand');
                jQuery(this).addClass('osb-map-min').removeClass('osb-map-max');
                jQuery(this).data('state', 'min');
            }
            osb.map.map.invalidateSize();
        });

        osb.populate(osb.config.rawDataCacheKey, osb.config.mapper.allPlaces());
    },

    /**
     * Get data that matches keywords - not cached.
     *
     * @param keywords - keywords to match on.
     */
    populateByKeywords: function (keywords) {
        osb.populate(null, osb.config.mapper.allPlacesKeywords(keywords)); // don't cache
    },

    /**
     * Set up the sidebar - show layers, keywords and handle interactions.
     */
    initSideBar: function () {

        // handle the hide / show of the sidebar
        jQuery('#osb-map-filter-header').on('click', function () {
            osb.toggleFilter();
        });

        var listHtml = '<ul>';

        // populate and handle the layer list
        for (var i = 0; i < osb.map.layers.length; i++) {
            // make sure the first later is assigned a style
            var selected = (i === 0) ? ' osb-selected-layer' : '';
            listHtml += '<li class="osb-map-layer">' +
                '<a class="osb-layer-link' + selected + '" data-id="' + osb.map.layers[i].id + '" href="#' + osb.map.layers[i].id +
                '">' + osb.map.layers[i].name + '</a></li>';
        }

        listHtml += '</ul>';

        jQuery('#osb-layer-list').html(listHtml);

        jQuery('.osb-map-layer').find("> a").off('click').on('click', function (e) {
            e.preventDefault();

            // remove selected layer styles
            jQuery('.osb-map-layer').find("> a").removeClass('osb-selected-layer');

            // add selected later style to this
            jQuery(this).addClass('osb-selected-layer');

            var id = jQuery(this).data('id');

            var layer = osb.map.layers.filter(function (obj) {
                return obj.id === id;
            });

            var selectedLayer = osb.map.createLayer(layer[0]);

            osb.map.map.removeLayer(osb.map.currentLayer);
            selectedLayer.addTo(osb.map.map);
            osb.map.currentLayer = selectedLayer;

            if (osb.map.map._zoom > osb.map.map._layersMaxZoom) {
                osb.map.map.setZoom(osb.map.map._layersMaxZoom);
            }
        });

    },

    toggleFilter: function () {
        // handle the hide / show of the sidebar
//        jQuery("#osb-map-filter-content").toggle();
        jQuery("#osb-map-wrapper").toggleClass('filter-x filter-y');
        setTimeout(function () {
            osb.map.map.invalidateSize();
        }, 800);
    },

    /**
     * Get the data about a place from cache or remote server.
     *
     * @param id - the ID of the place
     * @param displayFn - the function that displays the results
     */
    populatePlace: function (id, displayFn) {

        data = osb.cache.get(osb.config.placeDataCacheKey + '-' + id);

        if (data) {
            osb.handlePlaceData(data, displayFn);
        } else {
            // get the data
            osb.endpoint.getData(osb.config.mapper.place(id)).done(function (data) {
                osb.CacheAudit(osb.config.placeDataCacheKey);
                osb.cache.add(osb.config.placeDataCacheKey + '-' + id, data);
                osb.handlePlaceData(data, displayFn);
            });
        }
    },

    /**
     * Get the media associated with a place from the cache or a remote server.
     *
     * @param id - the ID of the place
     * @param mediaIds - the media IDs for a place
     * @param displayFn - the function to display the results
     */
    populateMedia: function (id, mediaIds, displayFn) {

        data = osb.cache.get(osb.config.mediaDataCacheKey + '-' + id);

        if (data) {
            osb.handleMediaData(data, displayFn);
        } else {
            // get the data
            osb.endpoint.getData(osb.config.mapper.media(mediaIds)).done(function (data) {
                osb.CacheAudit(osb.config.mediaDataCacheKey);
                osb.cache.add(osb.config.mediaDataCacheKey + '-' + id, data);
                osb.handleMediaData(data, displayFn);
            });
        }
    },


    /**
     * Call back function used with a successful ajax request.
     *
     * @param data - the data received from the server.
     */
    handleRawData: function (data) {
        jQuery('#osb-map-message').hide();
        osb.data.places = osb.config.mapper.mapAllPlacesResults(data);
        osb.populateMap();
        osb.populateKeyword();
        osb.updatePlaceTotal();
    },

    handlePlaceData: function (data, displayFn) {
        var placeData = osb.config.mapper.mapPlaceResult(data);
        displayFn(placeData);
    },


    handleMediaData: function (data, displayFn) {
        var mediaData = osb.config.mapper.mapMediaResults(data);
        displayFn(mediaData);
    },


    /**
     * Populate the map with markers and setup handlers for dealing with selecting points.
     */
    populateMap: function () {

        // we are going to cluster points
        if (osb.map.markers === null) {
            osb.map.markers = L.markerClusterGroup();
        } else {
            osb.map.markers.clearLayers();
        }

        if (osb.map.currentLayer === null) {
            var tileLayer = osb.map.createLayer(osb.map.layers[0]);
            osb.map.currentLayer = tileLayer;
            tileLayer.addTo(osb.map.map);
        }

        for (var i = 0; i < osb.data.places.length; i++) {
            var marker = L.marker([osb.data.places[i].lat, osb.data.places[i].lon], {icon: osb.map.icon});
            marker.osb_id = osb.data.places[i].id;
            marker.bindPopup(osb.popupForPlace(osb.data.places[i]));
            osb.map.markers.addLayer(marker);
        }

        // add the cluster to the map
        osb.map.map.addLayer(osb.map.markers);

        // resize map to fit the bounds
        if (osb.map.markers.getLayers().length > 0) {
            osb.map.map.fitBounds(osb.map.markers.getBounds());
        }

        // pull more data if the marker is selected
        osb.map.map.on('popupopen', function (e) {

            // center popup
            var cM = osb.map.map.project(e.popup._latlng);
            cM.y -= e.popup._container.clientHeight/2 + 100;
            osb.map.map.setView(osb.map.map.unproject(cM));
            osb.map.selectedPopup = e.popup;

            osb.map.selectedPopup = e.popup;

            // get the data id
            var id = jQuery(e.popup.getContent()).data('id');

            window.location.hash = 'osb/kyp/' + id;

            // get the media id values
            var mediaIds = jQuery(e.popup.getContent()).data('media-id');

            // handle the more ...
            jQuery('.osb-more').off('click').on('click', function (e) {
                //e.preventDefault();
                osb.displayPlace(id, mediaIds);
            });

            // get the place details
            osb.populatePlace(id, function (obj) {

                // limit the amount of text shown on the popup
                var desc = obj.description.substr(0, osb.config.truncate);
                if (/^\S/.test(obj.description.substr(osb.config.truncate))) {
                    desc = desc.replace(/\s+\S*$/, "") + ' ...';
                }

                // update the html
                jQuery('div.osb-popup-wrapper[data-id="' + id + '"]')
                    .find('> div.osb-place-details-wrapper > div.osb-place-details').html('<div>' + desc + '</div>');

            });

            // get media and add thumbnail, if appropriate
            if (mediaIds !== null) {
                osb.mediaForPlacePopup(id, mediaIds);
            }
        });

        osb.map.map.on('popupclose', function () {
            //window.location.hash = '';
        });

        osb.hashRoute();

        jQuery(window).off('hashchange').on('hashchange', function () {
            osb.hashRoute();
        });
    },

    /**
     * Create the basic popup information to get the details of a place. We use data attributes
     * to track the ARC GIS object IDs for the place and associated media.
     *
     * @param data - an object with the basic information.
     * @returns {string}
     */
    popupForPlace: function (data) {
        var popup = '<div class="osb-popup-wrapper" data-id="' + data.id + '" data-media-id="' +
            data.media.toString() + '">' +
            '<h4><a href="#osb/kyp/' + data.id + '/details" class="osb-more" data-id="' + data.id + '">' + data.title + '</a></h4>' +
            '<div class="osb-place-details-wrapper">' +
            '<div class="osb-place-details-image" style="text-align: center"></div>' +
            '<div class="osb-place-details"><div class="spinner medium"></div></div>' +
            '<div class="osb-popup-more"><a class="osb-more" data-id="' + data.id + '" href="#osb/kyp/' + data.id + '/details">' +
            'Read More</a></div>';
        if (data.keywords !== null) {
            popup += '<div class="osb-place-keywords"><p>Keywords: ' + data.keywords + '</p></div>';
        }
        popup += '</div>';
        return L.popup({maxWidth: 270, autoPan: true, closeOnClick: false}).setContent(popup);
        //return popup;
    },

    /**
     * Get the media for a place and, if there is an image, add it as a thumbnail to the
     * popup window. We get all the data details, which are cached, so are available if the
     * user clicks through to see all the details.
     *
     * @param placeId - the ID of the place
     * @param mediaIds - the ID values of the media
     */
    mediaForPlacePopup: function (placeId, mediaIds) {
        osb.populateMedia(placeId, mediaIds, function (data) {

            var img = null;

            // loop until we get an image
            for (var i = 0; i < data.length; i++) {
                if (data[i].mime === 'image/jpeg') {
                    img = '<div data-place-id="' + placeId +
                        '" style="display: inline-block" class="osb-image-spinner spinner medium"></div>' +
                        '<img class="osb-image" style="display: none" src="' + data[i].url + '"/>';
                    break;
                }
            }

            // if we have an image, show spinner until it loads
            if (img !== null) {
                jQuery('div.osb-popup-wrapper[data-id="' + placeId + '"]')
                    .find('> div.osb-place-details-wrapper > div.osb-place-details-image').html(img);
                jQuery('.osb-image').on('load', function () {
                    jQuery('div.osb-image-spinner[data-place-id="' + placeId + '"]').hide();
                    jQuery(this).show();
                });
            }

        });
    },

    /**
     * Populate the filter with keywords that are available within the available places.
     * We find unique keywords, count the duplicates and reorder them alphabetically. We
     * apply a threshold (low, medium and high) so we can apply a CSS style so popular
     * keywords can appear larger.
     */
    populateKeyword: function () {

        var selectedKeywords = osb.selectedKeywords();

        // to hold the list
        var keywordHtml = '<ul>';

        // clear existing results
        osb.keywords.data = {};

        // process keywords
        for (var i = 0; i < osb.data.places.length; i++) {
            if (osb.data.places[i].keywords !== null) {
                var tmp = osb.data.places[i].keywords.split(',');
                for (var j = 0; j < tmp.length; j++) {
                    var keyVal = tmp[j].trim();
                    if (osb.keywords.data.hasOwnProperty(keyVal)) {
                        osb.keywords.data[keyVal]++;
                    } else {
                        osb.keywords.data[keyVal] = 1;
                    }
                }
            }
        }

        // order alphabetically
        var keysSorted = Object.keys(osb.keywords.data).sort(
            function (a, b) {
                var a1 = a.toLowerCase();
                var b1 = b.toLowerCase();
                if (a1 < b1) return -1;
                else if (a1 > b1) return +1;
                else return 0;
            });

        for (var k = 0; k < keysSorted.length; k++) {
            if (selectedKeywords.indexOf(keysSorted[k]) === -1 && osb.keywords.data[keysSorted[k]] > 1) {
                keywordHtml += '<li class="' +
                    osb.keywords.threshold(osb.keywords.data[keysSorted[k]]) +
                    '"><a href="#">' + keysSorted[k] + '</a></li>';
            }
        }

        keywordHtml += '</ul>';

        jQuery('#osb-keyword-list').html(keywordHtml).find("> ul > li > a").off("click").on('click', function (e) {
            e.preventDefault();

            osb.handleSelectKeyword(jQuery(this).text());
            //e.stopPropagation();
        });
    },

    handleSelectKeyword: function (keyword) {

        // clear the original list
        jQuery('#osb-keyword-list').find('> ul ').empty();

        // update the selected list
        jQuery('#osb-selected-keyword-list').find("> ul").append('<li><span>' + keyword +
            '</span> [ <a href="#">x</a> ]</li>');

        osb.handleDeselectKeyword();


        // query and update the map
        osb.populateByKeywords(osb.selectedKeywords());
    },

    handleDeselectKeyword: function () {

        jQuery('#osb-selected-keyword-list').find("> ul > li > a").off('click').on('click', function () {
            jQuery(this).parent().remove();
            osb.populateByKeywords(osb.selectedKeywords());
        });

        // // clear the original list
        // jQuery('#osb-keyword-list').find('> ul ').empty();
        //
        // // update the selected list
        // jQuery('#osb-selected-keyword-list').find("> ul").append('<li><span>' + keyword + '</span> [ x ]</li>');

        // query and update the map

    },


    selectedKeywords: function () {

        var tmp = [];
        jQuery('#osb-selected-keyword-list').find('ul > li >span').each(
            function () {
                tmp.push(jQuery(this).text());
            });

        return tmp;
    },

    updatePlaceTotal: function () {

        var total = osb.data.places.length;

        var txt = total + ((total === 1) ? " place" : " places") + " available.";

        jQuery('#osb-places-total').find('>p').text(txt);

    },


    /**
     * Interim solution? We cache the place details, but no more than 10.
     */
    CacheAudit: function (cacheKey) {

        if (osb.cache.supported()) {

            // keep track of place keys
            var cacheKeys = [];

            // iterate the keys and track ones related to a place
            for (var i = 0; localStorage.key(i); i++) {
                var key = localStorage.key(i);
                if (key.substr(0, cacheKey.length) === cacheKey) {
                    cacheKeys.push(key);
                }
            }

            // clear the cache of places if we hit a limit
            if (cacheKeys.length >= osb.config.maxCacheItems) {
                for (var j = 0; j < cacheKeys.length; j++) {
                    osb.cache.remove(cacheKeys[j]);
                }
            }
        }
    },

    /**
     * Display the details about the place.
     *
     * @param id - the ID for a place
     * @param mediaIds - list of media IDs
     */
    displayPlace: function (id, mediaIds) {

        // hide the map
        jQuery('#osb-map-wrapper').hide();

        // show the details page
        jQuery('#osb-place-details').show();

        // hide the map description
        jQuery('#osb-map-description').hide();
        
        // handler for closing the details
        jQuery('.osb-desc-close').off().on('click', function (e) {

            e.preventDefault();

            osb.hidePlaceDetails();

            jQuery('#osb-map-description').show();
        });

        // get the details
        osb.populatePlace(id, function (obj) {

            // title
            var text = '<h3>' + obj.title + '</h3>';
            text += '<div class="osb-desc-wrapper">' + obj.fullDescription + '</div>';

            // description
            if (osb.date.hasDate(obj.dates)) {
                text += '<div class="osb-date"><p><strong>Date:</strong> ' + osb.date.dateText(obj.dates) + '</p></div>';
            }

            // keywords
            if (obj.keywords !== null) {
                text += '<div class="osb-keywords"><p><strong>Keywords:</strong> ' + obj.keywords + '</p></div>';
            }

            // add the content to the DOM
            jQuery('#osb-place-description-content').html(text);

            if (mediaIds !== null) {
                osb.populateMedia(id, mediaIds, osb.displayMediaForPlace);
            }


        });

    },

    displayMediaForPlace: function (data) {

        var out = '';

        for (var i = 0; i < data.length; i++) {

            if (data[i].title.length > 0) {

                var caption_text = data[i].caption || data[i].title + '';
                var alt_text = data[i].alt || data[i].title + '';

                out += '<h4 data-id="' + data[i].id + '">' + caption_text + '</h4>';

                if (data[i].mime.substr(0, 5) === 'image') {
                    out += '<div class="osb-img-wrapper">' +
                        '<div class="spinner medium_wbg"></div>' +
                        '<img class="osb-place-img" style="display: none" src="' + data[i].url + '" alt="' + alt_text + '"/>' +
                        '</div>';
                } else if (data[i].mime.substr(0, 5) === 'audio') {
                    out += '<audio controls="controls" preload="metadata"><source src="' + data[i].url + '" ' +
                        'type="' + data[i].mime + '"></audio>';
                } else if (data[i].mime.substr(0, 5) === 'video') {
                    out += '<video controls="controls" width="300" preload="metadata"><source src="' +
                        data[i].url + '" type="' + data[i].mime + '"></video>';
                } else {
                    out += '<p><a href="' + data[i].url + '">Open media (' + data[i].mime + ')</a></p>';
                }

                if (data[i].description !== null) {
                    out += '<p class="osb-image-description">' + data[i].description + '</p>';
                }
            }

        }
        jQuery('#osb-place-media').html(out);

        jQuery('img.osb-place-img').on('load', function () {
            jQuery(this).show();
            jQuery(jQuery(this).parent()).find('div.spinner').hide();
        });
    },


    /**
     * Check if media and video are playing and stop. Used when existing a page.
     */
    stopMediaPlaying: function () {

        jQuery.each(jQuery('audio'), function () {
            this.pause();
        });

        jQuery.each(jQuery('video'), function () {
            this.pause();
        });
    },

    hidePlaceDetails: function () {
        osb.stopMediaPlaying();
        jQuery('#osb-map-wrapper').show();
        jQuery('#osb-place-description-content').find('h3').text('');
        jQuery('#osb-place-details').hide();
        osb.map.map.invalidateSize();
        // if (osb.map.selectedPopup) {
        //     osb.map.map.openPopup(osb.map.selectedPopup);
        // }

    },

    findMarkerInCluster: function (id, fn) {
        var markers = osb.map.markers.getLayers();
        for (var i = 0; i < markers.length; i++) {
            if (markers[i].osb_id == id) {
                fn(markers[i]);
            }
        }
    },

    openPopupInCluster: function (marker) {

        // unclean! Added a delay to stop CSS transitions timing issues,
        // otherwise the popup is closed as soon as it has opened :-(
        setTimeout(function () {
            osb.map.markers.zoomToShowLayer(marker, function () {
                marker.openPopup();
            });
        }, 1000);
    },

    hashRoute: function () {
        if (window.location.hash) {
            // split on forward slashes ...
            var tmp = window.location.hash.split('/');
            // we should have at least three values ...
            if (tmp.length >= 3) {
                // osb should be our first value
                if (tmp[0] === "#osb") {
                    // we are dealing with a kyp id
                    if (tmp[1] === 'kyp') {
                        if (tmp.length == 3) { // fourth value would suggest showing details
                            if (jQuery("#osb-place-description").is(':visible') === true) {
                                osb.hidePlaceDetails();
                            }
                        }
                        osb.findMarkerInCluster(tmp[2], osb.openPopupInCluster);
                    }
                }

            }

        }
    },

    /**
     * We deal with specified dates and estimated dates - this might be a single date or a date range.
     * These variables and methods help to determine how this data is displayed.
     */
    date: {

        /**
         * Map value to month.
         */
        month: {
            '1': 'January', '2': 'February', '3': 'March', '4': 'April', '5': 'May', '6': 'June', '7': 'July',
            '8': 'August', '9': 'September', '10': 'October', '11': 'November', '12': 'December'
        },

        /**
         * Map value to year description.
         */
        year_estimate: {
            '1100|1199': '12th Century', '1200|1299': '13th Century', '1300|1399': '14th Century',
            '1400|1499': '15th Century', '1500|1599': '16th Century', '1600|1699': '17th Century',
            '1700|1799': '18th Century', '1800|1899': '19th Century', '1900|1949': 'Early 20th Century',
            '1940|1949': '1940s', '1950|1959': '1950s', '1950|1999': 'Late 20th Century', '1960|1969': '1960s',
            '1970|1979': '1970s', '1980|1989': '1980s', '1990|1999': '1990s', '2000|2049': 'Early 21st Century'
        },

        /**
         * Map value to month.
         */
        month_estimate: {'2|4': 'Spring', '5|7': 'Summer', '8|10': 'Autumn', '11|1': 'Winter'},

        /**
         * Map value to date description.
         */
        day_estimate: {
            '0|12': 'The start of the month',
            '10|20': 'The middle of the month',
            '18|31': 'The end of the month'
        },

        /**
         * Create a date string based on specified dates.
         *
         * @param date
         * @returns {string}
         */
        dateSpecified: function (date) {

            var dateText = "";

            if (date.day) {
                dateText += date.day + " ";
            }
            if (date.month) {
                dateText += this.month[date.month] + " ";
            }
            if (date.year) {
                dateText += date.year;
            }
            return dateText;
        },

        /**
         * Create a date string based on estimated dates.
         *
         * @param date
         * @returns {string}
         */
        dateEstimated: function (date) {

            var dateText = "";

            if (date.est_day) {
                dateText += this.day_estimate[date.est_day] + " ";
            }
            if (date.est_month) {
                dateText += this.month_estimate[date.est_month] + " ";
            }
            if (date.est_year) {
                dateText += this.year_estimate[date.est_year];
            }
            return dateText;
        },

        hasDate: function (date) {
            return this.hasSpecified(date.from) || this.hasEstimated(date.from) ||
                this.hasSpecified(date.to) || this.hasEstimated(date.to);
        },

        hasRange: function (date) {
            return (this.hasSpecified(date.from) || this.hasEstimated(date.from)) &&
                (this.hasSpecified(date.to) || this.hasEstimated(date.to));
        },

        hasSpecified: function (date) {
            return date.day !== null || date.month !== null || date.year !== null;
        },

        hasEstimated: function (date) {
            return date.est_day !== null || date.est_month !== null || date.est_year !== null;
        },

        dateText: function (date) {

            var dateText = "";

            if (this.hasSpecified(date.from)) {
                dateText += this.dateSpecified(date.from);
            } else if (this.hasEstimated(date.from)) {
                dateText += this.dateEstimated(date.from);
            }

            if (this.hasRange(date)) {
                dateText += ' - ';
            }

            if (this.hasSpecified(date.to)) {
                dateText += this.dateSpecified(date.to);
            } else if (this.hasEstimated(date.to)) {
                dateText += this.dateEstimated(date.to);
            }

            return dateText;
        }
    },

    /**
     * Used to query the ArcGIS system.
     */
    endpoint: {

        getData: function (url) {
            return jQuery.ajax({
                dataType: 'json',
                type: 'GET',
                url: url
            });
        }

    },


    /**
     * We use local storage to cache data to help with performance. We check that local storage is available
     * before using, since it might not be available in private browsing.
     */
    cache: {

        /**
         * Add an item to the cache. We convert the object to a string and ee add a timestamp so we can track
         * the age of the item.
         *
         * @param key - key to access the item
         * @param data - data to store
         */
        add: function (key, data) {
            if (this.supported()) {
                localStorage.removeItem(key);
                localStorage.setItem(key, JSON.stringify({timestamp: new Date().getTime(), data: data}));
            }
        },

        /**
         * Get an item from the cache. We check that the age of the data
         *
         * @param key - key to access the item
         * @returns json representation of data or null
         */
        get: function (key) {
            if (this.supported()) {
                var cacheData = localStorage.getItem(key);
                if (cacheData !== null) {
                    var obj = JSON.parse(cacheData);
                    if (!this.expired(obj.timestamp)) {
                        return obj.data;
                    }
                }
            }
            return null;
        },

        /**
         * Remove an item from the cache.
         *
         * @param key - key to access the item
         */
        remove: function (key) {
            if (this.supported()) {
                localStorage.removeItem(key);
            }
        },

        /**
         * Clear the cache.
         */
        clear: function () {
            if (this.supported()) {
                localStorage.clear();
            }
        },

        /**
         * Has the object expired?

         * @param timestamp - the timestamp attached to the cached object.
         * @returns {boolean}
         */
        expired: function (timestamp) {
            return new Date().getTime() > (timestamp + osb.config.dataCacheExpire);
        },

        /**
         * Can we access local storage?
         *
         * @returns {boolean}
         */
        supported: function () {
            return typeof Storage !== 'undefined';
        }

    }
};


var mapper = {

    /**
     * Fields used to populate map markers etc.
     */
    allPlacesFields: 'OBJECTID,ID,COORDINATES_LATITUDE,COORDINATES_LONGITUDE,ID,TITLE,KEYWORDS',

    /**
     * Fields with data for an individual place.
     */
    placeField: 'OBJECTID,TITLE,KEYWORDS,DESCRIPTION,DESCRIPTION_FULL,DATE_ACTUAL_DAY,DATE_ACTUAL_MONTH,' +
    'DATE_ACTUAL_YEAR,DATE_EST_DAY_ESTIMATE,DATE_EST_MONTH_ESTIMATE,DATE_EST_YEAR_ESTIMATE,TO_DATE_ACTUAL_DAY,' +
    'TO_DATE_ACTUAL_MONTH,TO_DATE_ACTUAL_YEAR,TO_DATE_EST_DAY_ESTIMATE,TO_DATE_EST_MONTH_ESTIMATE,' +
    'TO_DATE_EST_YEAR_ESTIMATE',

    mediaField: 'OBJECTID,MEDIA_LIST_MEDIA_ID,MEDIA_LIST_MEDIA_TITLE,MEDIA_LIST_MEDIA_DESCRIPTION,' +
    'MEDIA_LIST_MEDIA_MIME_TYPE,MEDIA_LIST_MEDIA_URL,MEDIA_LIST_MEDIA_CAPTION,MEDIA_LIST_MEDIA_ALT' +
    ',MEDIA_TITLE,MEDIA_DESCRIPTION,MEDIA_MIME_TYPE,MEDIA_URL,MEDIA_CAPTION,MEDIA_ALT',


    /**
     * The URL to request data from the ArcGIS system.
     *
     * @param query - SQL WHERE clause ... what could go wring!?
     * @param objectIds - an empty string or string of ID(s) separated by a comma.
     * @param fields - string of fields, separated by a comma.
     * @returns {string}
     */
    coreUrl: function (query, objectIds, fields) {
        return 'https://maps.bristol.gov.uk/arcgis/rest/services/ext/her_2020_v3/FeatureServer/43/query?where=' + query +
            '&objectIds=' + objectIds + '&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=' +
            '&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=' + fields +
            '&returnGeometry=true&maxAllowableOffset=&geometryPrecision=&outSR=&gdbVersion=' +
            '&returnDistinctValues=false&returnIdsOnly=false&returnCountOnly=false&orderByFields=&' +
            'groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&f=pjson';
    },

    encode: function (fragment) {
        return encodeURIComponent(fragment).replace(/'/g, "%27").replace(/"/g, "%22");
    },

    /**
     * URL that gets all places with their ID and title.
     *
     * Pass to the WHERE clause "ID like 'osb/place/%'".
     *
     * @returns {*|string}
     */
    allPlaces: function () {
        return mapper.coreUrl(this.encode("ID like 'osb/place/%'"), '', this.allPlacesFields);
    },

    allPlacesKeywords: function (keywords) {

        if (keywords.length === 0) return mapper.allPlaces();

        var where = '(';

        for (var i = 0; i < keywords.length; i++) {
            if (i > 0) {
                where += 'AND ';
            }
            where += "KEYWORDS LIKE '%" + keywords[i] + "%' ";
        }

        where += ") AND ID like 'osb/place/%'";

        return mapper.coreUrl(this.encode(where), '', this.allPlacesFields);

    },

    /**
     *
     * URL that gets more details about a single place.
     * @param id
     * @returns {*|string}
     */
    place: function (id) {
        return mapper.coreUrl('', id, this.placeField);
    },

    media: function (ids) {
        return mapper.coreUrl('', ids, this.mediaField);
    },

    /**
     * Map the raw data to something we can use.
     *
     * @param data - the data received from the server.
     * @returns {Array}
     */
    mapAllPlacesResults: function (data) {

        var trackPlaceIds = [];

        var result = [];

        if (data && data.features) {

            for (var i = 0; i < data.features.length; i++) {
                if (trackPlaceIds.indexOf(data.features[i].attributes.ID) === -1) {
                    //noinspection JSUnresolvedVariable
                    result.push({
                        id: data.features[i].attributes.OBJECTID,
                        osb_id: data.features[i].attributes.ID,
                        lat: data.features[i].attributes.COORDINATES_LATITUDE,
                        lon: data.features[i].attributes.COORDINATES_LONGITUDE,
                        title: data.features[i].attributes.TITLE,
                        keywords: data.features[i].attributes.KEYWORDS,
                        media: [data.features[i].attributes.OBJECTID]
                    });
                    trackPlaceIds.push(data.features[i].attributes.ID);
                } else {
                    var index = trackPlaceIds.indexOf(data.features[i].attributes.ID);
                    //noinspection JSUnresolvedVariable
                    result[index].media.push(data.features[i].attributes.OBJECTID);
                }
            }
        }

        return result;
    },

    /**
     * Map the data for an individual place.
     *
     * @param data - the data received from the server.
     * @returns {{id: *, title: *, thumbnail: *, description: *}}
     */
    mapPlaceResult: function (data) {

        //noinspection JSUnresolvedVariable
        return {
            id: data.features[0].attributes.OBJECTID,
            title: data.features[0].attributes.TITLE,
            keywords: data.features[0].attributes.KEYWORDS,
            description: data.features[0].attributes.DESCRIPTION,
            fullDescription: data.features[0].attributes.DESCRIPTION_FULL,
            dates: {
                from: {
                    day: data.features[0].attributes.DATE_ACTUAL_DAY,
                    month: data.features[0].attributes.DATE_ACTUAL_MONTH,
                    year: data.features[0].attributes.DATE_ACTUAL_YEAR,
                    est_day: data.features[0].attributes.DATE_EST_DAY_ESTIMATE,
                    est_month: data.features[0].attributes.DATE_EST_MONTH_ESTIMATE,
                    est_year: data.features[0].attributes.DATE_EST_YEAR_ESTIMATE
                },
                to: {
                    day: data.features[0].attributes.TO_DATE_ACTUAL_DAY,
                    month: data.features[0].attributes.TO_DATE_ACTUAL_MONTH,
                    year: data.features[0].attributes.TO_DATE_ACTUAL_YEAR,
                    est_day: data.features[0].attributes.TO_DATE_EST_DAY_ESTIMATE,
                    est_month: data.features[0].attributes.TO_DATE_EST_MONTH_ESTIMATE,
                    est_year: data.features[0].attributes.TO_DATE_EST_YEAR_ESTIMATE
                }
            }

        };

    },

    /**
     * Due to the way that data is mapped, we get duplicate media. To weed out the duplicates we can
     * look at the URLs. However, the URL might be in the MEDIA_URL or the MEDIA_LIST_MEDIA_URL!
     *
     * @param data - raw data with duplicates.
     * @returns {Array} - array without the duplicates.
     */
    mapMediaResults: function (data) {

        var media = [];
        var trackMediaUrls = [];

        for (var i = 0; i < data.features.length; i++) {
            //noinspection JSUnresolvedVariable
            if (trackMediaUrls.indexOf(data.features[i].attributes.MEDIA_LIST_MEDIA_URL) == -1 &&
                trackMediaUrls.indexOf(data.features[i].attributes.MEDIA_URL) == -1) {
                //noinspection JSUnresolvedVariable
                media.push({
                    id: data.features[i].attributes.OBJECTID,
                    title: data.features[i].attributes.MEDIA_LIST_MEDIA_TITLE || data.features[i].attributes.MEDIA_TITLE || '',
                    description: data.features[i].attributes.MEDIA_LIST_MEDIA_DESCRIPTION || data.features[i].attributes.MEDIA_DESCRIPTION || '',
                    mime: data.features[i].attributes.MEDIA_LIST_MEDIA_MIME_TYPE || data.features[i].attributes.MEDIA_MIME_TYPE || '',
                    url: data.features[i].attributes.MEDIA_LIST_MEDIA_URL || data.features[i].attributes.MEDIA_URL || '',
                    caption: data.features[i].attributes.MEDIA_LIST_MEDIA_CAPTION || data.features[i].attributes.MEDIA_CAPTION || '',
                    alt: data.features[i].attributes.MEDIA_LIST_MEDIA_ALT || data.features[i].attributes.MEDIA_ALT || ''
                });
                //noinspection JSUnresolvedVariable
                trackMediaUrls.push(data.features[i].attributes.MEDIA_LIST_MEDIA_URL || data.features[i].attributes.MEDIA_URL);
            }
        }

        return media;
    }

};